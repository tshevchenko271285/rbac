# RBAC
Role Based Access Control

## Installation
```
composer require tshevchenko/rbac
```

## Migration
Running migrations to create tables in the database
```
php artisan migrate
```

## ENV constants required:
- `RBAC_SUPER_ADMIN_EMAIL` - The super admin email used by Seeds to assign the **Super Admin** role.
- `RBAC_USER_MODEL` - The fully qualified class name of the User model (`App\Models\User`)

## Config
Publish config file
```
php artisan vendor:publish --tag=rbac-config
```
Brief description of settings:
- `storage` - May be `db` or `config`. In the case of `db` the permission will be stored in the database.
  In the case of `config`, the permissions will be taken from the `permissions` property of that configuration file. It might be convenient
  for small projects with a small number of protected sections.
- `super_admin_email` - see ENV `RBAC_SUPER_ADMIN_EMAIL`
- `user_model` - see ENV `RBAC_USER_MODEL`
- `super_admin_role_id` - The Role ID **Super Admin**. This role is granted access without additional checks.
- `permissions` - The array of permissions. Each array element must contain `role_id` and `action`
    ```
    'permissions' => [
        [
            'role_id' => 4,
            'action' => 'App\Http\Controllers\ProfileController@edit',
        ],
    ],
    ```
- `excluded_actions` - An array of routes that will be excluded when updating `rbac_actions` table.
    ```
    'excluded_actions' => [
        'generated::',
        'ignition.',
        'sanctum.',
    ],
    ```

## Trait
Use `Tshevchenko\Rbac\Models\Traits\RbacTrait` in your User model. 

## Commands
Use command `rbac:actions-update` to update table `rbac_actions`. This will update the table according to the existing routes. Always use this command after adding new routes to your application.

## Seeds
The sedding data creates 4 access roles to demonstrate capabilities.
- **Super Admin**, has access to all protected sections, rights to specific actions are not checked.
- Admin, Manager, **User** - created as an example, the **User** role is assigned to all existing users.

```
php artisan db:seed --class=Tshevchenko\\Rbac\\Database\\seeders\\RoleSeeder
```
