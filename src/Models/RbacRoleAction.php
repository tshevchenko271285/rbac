<?php

namespace Tshevchenko\Rbac\Models;

use Illuminate\Database\Eloquent\Attributes\ObservedBy;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Tshevchenko\Rbac\Observers\RbacRoleActionObserver;

#[ObservedBy([RbacRoleActionObserver::class])]
class RbacRoleAction extends Pivot
{}
