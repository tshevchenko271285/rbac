<?php

namespace Tshevchenko\Rbac\Models\Traits;

use Tshevchenko\Rbac\Models\RbacRole;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Tshevchenko\Rbac\Services\ActionService;

trait RbacTrait
{
    /**
     * Returns the roles of this user
     *
     * @return BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(RbacRole::class, 'rbac_role_user', 'user_id', 'role_id');
    }

    /**
     * Checks the user's permissions for the route action
     *
     * @param string $action
     * @return bool
     */
    public function hasAccess(string $action): bool
    {
        $userRolesID = $this->loadMissing('roles')
            ->roles
            ->pluck('id')
            ->toArray();

        // Does`n have any roles
        if (empty($userRolesID)) return false;

        // The current User is a Super Admin
        if (in_array(config('rbac.super_admin_role_id'), $userRolesID)) return true;

        $userRoleActions = array_filter(
            ActionService::getActions(),
            fn ($k) => in_array($k, $userRolesID),
            ARRAY_FILTER_USE_KEY
        );

        $userActions = array_unique(array_merge(...$userRoleActions));

        return in_array($action, $userActions);
    }
}
