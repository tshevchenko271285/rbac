<?php

namespace Tshevchenko\Rbac\Http\Requests;

use App\Http\Requests\Admin\CommonRequest;

class RbacRoleRequest extends CommonRequest
{
    protected function postRules(): array
    {
        return [
            'title' => ['required', 'string', 'max:255', 'min:3', 'unique:rbac_roles,title'],
            'actions' => ['array'],
        ];
    }

    protected function putRules(): array
    {
        return [
            'title' => [
                'required',
                'string',
                'max:255',
                'min:3',
                'unique:rbac_roles,title,' . $this->role->id,
            ],
            'actions' => ['array'],
        ];
    }
}
