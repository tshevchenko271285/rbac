<?php

namespace Tshevchenko\Rbac\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Tshevchenko\Rbac\Http\Requests\RbacRoleRequest;
use Tshevchenko\Rbac\Models\RbacAction;
use Tshevchenko\Rbac\Models\RbacRole;

class RbacRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        return view('rbac::role.index', [
            'roles' => RbacRole::paginate(10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Renderable
     */
    public function create(): Renderable
    {
        return view('rbac::role.create', [
            'actions' => RbacAction::orderBy('name')->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RbacRoleRequest $request
     * @return RedirectResponse
     */
    public function store(RbacRoleRequest $request): RedirectResponse
    {
        try {
            $data = $request->validated();

            $role = RbacRole::create($data);

            $role->actions()->sync($request->actions);

            return redirect()
                ->route(config('rbac.route_name') . 'role.edit', $role)
                ->with('success', __('Successfully created'));
        } catch (\Exception $e) {
            return redirect()
                ->route(config('rbac.route_name') . 'role.create')
                ->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param RbacRole $role
     * @return Renderable
     */
    public function edit(RbacRole $role): Renderable
    {
        $role->load('actions');

        return view('rbac::role.edit', [
            'role' => $role,
            'actions' => RbacAction::orderBy('name')->get(),
            'selected_actions_id' => $role->actions->pluck('id')->toArray(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RbacRoleRequest $request
     * @param RbacRole $role
     * @return RedirectResponse
     */
    public function update(RbacRoleRequest $request, RbacRole $role): RedirectResponse
    {
        try {
            $role->update(['title' => $request->title]);

            $role->actions()->sync($request->actions);

            return redirect()
                ->route(config('rbac.route_name') . 'role.edit', $role)
                ->with('success', __('Successfully updated'));
        } catch (\Exception $e) {
            return redirect()
                ->route('admin.role.edit', $role)
                ->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param RbacRole $role
     * @return RedirectResponse
     */
    public function destroy(RbacRole $role): RedirectResponse
    {
        $role->delete();

        return redirect()
            ->route(config('rbac.route_name') . 'role.index')
            ->with('success', __('Successfully deleted'));
    }
}
