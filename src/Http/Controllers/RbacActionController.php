<?php

namespace Tshevchenko\Rbac\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Tshevchenko\Rbac\Models\RbacAction;
use Tshevchenko\Rbac\Services\ActionService;

/**
 * Class RbacActionController
 *
 * @package Tshevchenko\Rbac\Http\Controllers
 */
class RbacActionController extends Controller
{
    const FORCE_REMOVE_KEY = 'force_remove';

    const REMOVE_KEY = 'remove';

    /**
     * Display a listing of the resource.
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        $actions = RbacAction::withTrashed()
            ->orderBy('deleted_at')
            ->orderBy('name')
            ->get();

        $newRouteActions = array_values(array_diff(
            ActionService::getRouteActions(),
            $actions->pluck('name')->toArray()
        ));

        return view(
            'rbac::action.index',
            compact('actions', 'newRouteActions')
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Renderable
     */
    public function create(): Renderable
    {
        return view('rbac::action.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param FormRequest $request
     * @return RedirectResponse
     */
    public function store(FormRequest $request): RedirectResponse
    {
        $data = $request->validate([
            'name' => ['required', 'min:3', 'unique:rbac_actions,name']
        ]);

        try {
            $removeKey = self::REMOVE_KEY;

            if ($request->$removeKey === $removeKey) {
                $data['deleted_at'] = now();
            }

            $action = RbacAction::create($data);

            return redirect()
                ->route(config('rbac.route_name') . 'action.index', $action)
                ->with('success', __('Successfully created'));
        } catch (\Throwable $e) {
            return redirect()
                ->route(config('rbac.route_name') . 'action.create')
                ->withErrors([$e->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param RbacAction $action
     * @return Renderable
     */
    public function edit(RbacAction $action): Renderable
    {
        return view('rbac::action.edit', [
            'action' => $action,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param FormRequest $request
     * @param RbacAction $action
     * @return RedirectResponse
     */
    public function update(FormRequest $request, RbacAction $action): RedirectResponse
    {
        $data = $request->validate([
            'id' => ['required', 'exists:rbac_actions,id'],
            'name' => ['required', 'min:3', 'unique:rbac_actions,name,' . $action->id],
        ]);

        try {
            $action->update($data);

            return redirect()
                ->route(config('rbac.route_name') . 'action.edit', $action)
                ->with('success', __('Successfully updated'));
        } catch (\Throwable $e) {
            return redirect()
                ->route(config('rbac.route_name') . 'action.edit', $action)
                ->withErrors([$e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int $actionID
     * @return RedirectResponse
     */
    public function destroy(Request $request, int $actionID): RedirectResponse
    {
        try {
            $action = RbacAction::withTrashed()->findOrFail($actionID);

            $forceKey = self::FORCE_REMOVE_KEY;

            if ($request->$forceKey === $forceKey) {
                $action->forceDelete();
            } else {
                $action->delete();
            }

            return redirect()
                ->route(config('rbac.route_name') . 'action.index')
                ->with('success', __('Successfully deleted'));
        } catch (\Throwable $e) {
            return redirect()
                ->route(config('rbac.route_name') . 'action.index')
                ->withErrors([$e->getMessage()]);
        }
    }

    /**
     * @param int $actionID
     * @return RedirectResponse
     */
    public function restore(int $actionID): RedirectResponse
    {
        try {
            RbacAction::withTrashed()->findOrFail($actionID)->restore();

            return redirect()
                ->route(config('rbac.route_name') . 'action.index')
                ->with('success', __('Successfully deleted'));
        } catch (\Throwable $e) {
            return redirect()
                ->route(config('rbac.route_name') . 'action.index')
                ->withErrors([$e->getMessage()]);
        }
    }
}
