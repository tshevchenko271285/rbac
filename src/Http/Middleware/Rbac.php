<?php

namespace Tshevchenko\Rbac\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class Rbac
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(Request): (Response) $next
     * @return Response
     */
    public function handle(Request $request, Closure $next): Response
    {
        return $request->user()->hasAccess($request->route()->getName())
            ? $next($request)
            : abort(Response::HTTP_UNAUTHORIZED);
    }
}
