<?php

namespace Tshevchenko\Rbac;

use Tshevchenko\Rbac\Console\Commands\UpdateRoles;
use Tshevchenko\Rbac\Http\Middleware\Rbac;
use Illuminate\Support\ServiceProvider;

class RbacServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->load();

        $this->publish();

        app('router')->aliasMiddleware('rbac', Rbac::class);
    }

    private function load()
    {
        $this->loadMigrationsFrom(__DIR__.'/Database/migrations');

        $this->loadRoutesFrom(__DIR__.'/routes/web.php');

        $this->loadViewsFrom(__DIR__.'/resources/views', 'rbac');

        $this->commands([UpdateRoles::class]);
    }

    private function publish()
    {
        $this->publishes([
            __DIR__.'/config/rbac.php' => config_path('rbac.php'),
        ], 'rbac-config');

        $this->publishes([
            __DIR__.'/resources/views' => resource_path('views/vendor/rbac'),
        ], 'rbac-views');
    }
}
