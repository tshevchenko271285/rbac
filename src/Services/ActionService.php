<?php

namespace Tshevchenko\Rbac\Services;

use Tshevchenko\Rbac\Models\RbacRole;

class ActionService
{
    const CACHE_KEY = 'rbac_role_actions';

    const STORAGE_TYPE_DB = 'db';

    const STORAGE_TYPE_CONFIG = 'config';

    /**
     * Returns the role action group by role ID
     *
     * @return array
     */
    public static function getActions(): array
    {
        return config('rbac.storage', self::STORAGE_TYPE_CONFIG) === self::STORAGE_TYPE_DB
            ? \Cache::rememberForever(self::CACHE_KEY, fn () => self::cacheUpdate())
            : config('rbac.permissions', []);
    }

    /**
     * Returns exists actions from the app routes
     *
     * @return array
     */
    public static function getRouteActions(): array
    {
        $routes = \Route::getRoutes();

        $routesActions = array_keys($routes->compile()['attributes']);

        return array_values(array_filter($routesActions, function ($action) {
            foreach (config('rbac.excluded_route_actions') as $exclude) {
                if (strripos($action, $exclude) !== false)
                    return false;
            }

             return true;
        }));
    }


    /**
     * Updates the role cache and returns newly received actions.
     *
     * @return array
     */
    public static function cacheUpdate(): array
    {
        $rolesPermissions = RbacRole::with(['actions'])
            ->get()
            ->mapWithKeys(fn($role) => [$role->id => $role->actions->pluck('name')->toArray()])
            ->toArray();

        \Cache::put(self::CACHE_KEY, $rolesPermissions);

        return $rolesPermissions;
    }
}
