<?php

namespace Tshevchenko\Rbac\Observers;

use Tshevchenko\Rbac\Models\RbacRole;
use Tshevchenko\Rbac\Services\ActionService;

class RbacRoleObserver
{
    /**
     * Handle the Role "created" event.
     */
    public function created(): void
    {
        ActionService::cacheUpdate();
    }

    /**
     * Handle the Role "updated" event.
     */
    public function updated(): void
    {
        ActionService::cacheUpdate();
    }

    /**
     * Handle the Role "deleted" event.
     */
    public function deleted(): void
    {
        ActionService::cacheUpdate();
    }

    /**
     * Handle the Role "restored" event.
     */
    public function restored(): void
    {
        ActionService::cacheUpdate();
    }

    /**
     * Handle the Role "forceDeleted" event.
     */
    public function forceDeleted(): void
    {
        ActionService::cacheUpdate();
    }
}
