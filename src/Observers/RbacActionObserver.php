<?php

namespace Tshevchenko\Rbac\Observers;

use Tshevchenko\Rbac\Models\RbacAction;
use Tshevchenko\Rbac\Services\ActionService;

class RbacActionObserver
{
    /**
     * Handle the Action "updated" event.
     */
    public function updated(): void
    {
        ActionService::cacheUpdate();
    }

    /**
     * Handle the Action "deleted" event.
     */
    public function deleted(): void
    {
        ActionService::cacheUpdate();
    }

    /**
     * Handle the Action "restored" event.
     */
    public function restored(): void
    {
        ActionService::cacheUpdate();
    }

    /**
     * Handle the Action "forceDeleted" event.
     */
    public function forceDeleted(): void
    {
        ActionService::cacheUpdate();
    }
}
