<?php

namespace Tshevchenko\Rbac\Observers;

use Tshevchenko\Rbac\Models\RbacRoleAction;
use Tshevchenko\Rbac\Services\ActionService;

class RbacRoleActionObserver
{
    /**
     * Handle the RbacRoleAction Pivot "created" event.
     */
    public function created(): void
    {
        ActionService::cacheUpdate();
    }

    /**
     * Handle the RbacRoleAction Pivot "deleted" event.
     */
    public function deleted(): void
    {
        ActionService::cacheUpdate();
    }
}
