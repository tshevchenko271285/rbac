<?php

namespace Tshevchenko\Rbac\Console\Commands;

use Illuminate\Console\Command;
use Tshevchenko\Rbac\Models\RbacAction;
use Tshevchenko\Rbac\Services\ActionService;

class UpdateRoles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rbac:actions-update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the actions table';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        try {
            $actions = ActionService::getRouteActions();

            foreach ($actions as $name) {
                RbacAction::updateOrCreate(['name' => $name]);

                $this->info('Route ' . $name . ' updated');
            }

            $this->info('Routes updating successfully finished');
        } catch (\Throwable $e) {
            $this->info($e->getMessage());
        }
    }
}
