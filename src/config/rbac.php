<?php

use Tshevchenko\Rbac\Services\ActionService;

return [
    /*
    |--------------------------------------------------------------------------
    | Defines the role's action store type.
    |--------------------------------------------------------------------------
    |
    | Available values:
    | - ActionService::STORAGE_TYPE_DB - Actions associated with roles are stored in a database.
    | - ActionService::STORAGE_TYPE_CONFIG - Actions associated with roles are stored in
    |   the "permissions" array of the configuration file. The key corresponds to the role ID.
    |   Value is an array of actions available for a specific role.
    */
    'storage' => ActionService::STORAGE_TYPE_DB, // ActionService::STORAGE_TYPE_DB | ActionService::STORAGE_TYPE_CONFIG

    /*
    |--------------------------------------------------------------------------
    | Used when seeding data and assigning roles to current users.
    |--------------------------------------------------------------------------
    */
    'user_model' => \App\Models\User::class,

    /*
    |--------------------------------------------------------------------------
    | When seeding data, the user with this E-Mail will be assigned the role of Super Admin.
    |--------------------------------------------------------------------------
    */
    'super_admin_email' => env('RBAC_SUPER_ADMIN_EMAIL'),

    /*
    |--------------------------------------------------------------------------
    | Super Admin role ID.
    | Users with this role will be granted access to all actions.
    |--------------------------------------------------------------------------
    */
    'super_admin_role_id' => 1,

    /*
    |--------------------------------------------------------------------------
    | List of available actions for roles.
    |--------------------------------------------------------------------------
    */
    'permissions' => [
        1 => [ // Role id
            'admin.dashboard', // An actions
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | List of routes that need to be excluded from rbac actions.
    |--------------------------------------------------------------------------
    */
    'excluded_route_actions' => [
        'login',
        'logout',
        'register',
        'password.',
        'generated::',
        'ignition.',
        'sanctum.',
    ],

     /*
     |--------------------------------------------------------------------------
     | Router prefix
     |--------------------------------------------------------------------------
     */
    'route_prefix' => '/rbac',


    /*
    |--------------------------------------------------------------------------
    | Router name
    |--------------------------------------------------------------------------
    */
    'route_name' => 'rbac.',


    /*
    | Layout for displaying views.
    */
    'layout' => 'rbac::layouts.app',
];
