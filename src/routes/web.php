<?php

use Tshevchenko\Rbac\Http\Controllers\RbacActionController;
use Tshevchenko\Rbac\Http\Controllers\RbacRoleController;

Route::middleware(['web', 'auth', 'rbac'])
    ->prefix(config('rbac.route_prefix', '/rbac'))
    ->name(config('rbac.route_name', 'rbac.'))
    ->group(function () {
        Route::resource('action', RbacActionController::class)->except(['show']);
        Route::patch('action/restore/{action}', [RbacActionController::class, 'restore'])->name('action.restore');

        Route::resource('/role', RbacRoleController::class)->except(['show']);
    });
