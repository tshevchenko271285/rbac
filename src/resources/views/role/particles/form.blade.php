@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif

@if($successMessage = session('success'))
    <div class="alert alert-success">{{ $successMessage }}</div>
@endif

<div class="form-group">
    <label for="">{{ __('Role title') }} *</label>

    <input
        type="text"
        class="form-control"
        name="title"
        placeholder="{{ __('Role title') }}"
        value="{{ old('title', !empty($role) ? $role->title : "") }}"
    />
</div>

<div class="my-3">
    <a href="{{ route(config('rbac.route_name') . 'role.index') }}" class="btn btn-default">
        {{ __('Cancel') }}
    </a>

    <input type="submit" class="btn btn-primary" value="{{ __('Save') }}">
</div>

@if($actions)
    <table class="table">
        <thead>
            <tr>
                <th width="30">
                    <input
                        id="toggleAllActions"
                        class="form-check-input toggle-all-checkboxes"
                        type="checkbox"
                        data-toggle-all-checkboxes='input[type="checkbox"][id^="roleCheckbox"]'
                        {{ !empty($selected_actions_id) && count($selected_actions_id) === $actions->count() ? 'checked' : '' }}
                    />
                </th>

                <th>
                    <label for="toggleAllActions">
                        {{ __('Toggle all') }}
                    </label>
                </th>
            </tr>
        </thead>

        <tbody>
            @foreach($actions as $key => $action)
                @php
                    $checkboxID = 'roleCheckbox' . $key;
                @endphp

                <tr>
                    <td>
                        <div class="form-check">
                            <input
                                id="{{ $checkboxID }}"
                                class="form-check-input"
                                type="checkbox"
                                value="{{ $action->id }}"
                                name="actions[]"
                                {{ !empty($selected_actions_id) && in_array($action->id, $selected_actions_id) ? 'checked' : '' }}
                            />
                        </div>
                    </td>

                    <td>
                        <label class="form-check-label" for="{{ $checkboxID }}">
                            {{ $action->name }}
                        </label>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endif
