<div class="card">
    <div class="card-header border-0">
        <a href="{{ route(config('rbac.route_name') . 'role.index') }}" class="btn btn-sm btn-outline-warning me-1">
            <i class="fa-solid fa-chevron-left"></i>
        </a>

        {{ $title ?? '' }}
    </div>
</div>
