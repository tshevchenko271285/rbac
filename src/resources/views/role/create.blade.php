@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-auto mb-3">
                @include('rbac::role.particles.form-header', [
                    'title' => __('Create role'),
                ])
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 mx-auto">
                <form class="form-horizontal" action="{{ route(config('rbac.route_name') . 'role.store') }}" method="post">
                    {{ csrf_field() }}

                    @include('rbac::role.particles.form')

                    <hr/>

                    <a href="{{ route(config('rbac.route_name') . 'role.index') }}" class="btn btn-default">
                        {{ __('Cancel') }}
                    </a>

                    <input type="submit" class="btn btn-primary" value="{{ __('Save') }}">
                </form>
            </div>
        </div>
    </div>
@endsection
