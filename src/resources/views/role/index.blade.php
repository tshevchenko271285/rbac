@extends(config('rbac.layout', 'rbac::layouts.app'))

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="card">
                <div class="card-header">{{ __('Rbac Roles') }}</div>

                <div class="card-body">
                    <a href="{{ route(config('rbac.route_name') . 'role.create') }}" class="btn btn-primary">
                        {{ __('Create') }}
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 mx-auto">
            @if($roles->count())
                <table class="mt-5 table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Title</th>
                            <th scope="col" width="100"></th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($roles as $role)
                            <tr>
                                <td>
                                    <a href="{{ route(config('rbac.route_name') . 'role.edit', $role) }}">
                                        {{ $role->title }}
                                    </a>
                                </td>

                                <td class="action">
                                    <a
                                        href="{{ route(config('rbac.route_name') . 'role.edit', $role) }}"
                                        class="btn btn-sm btn-outline-primary"
                                    >
                                        <i class="fa-solid fa-user-pen"></i>
                                    </a>

                                    <form
                                        onsubmit="return confirm('{{ __('Delete?') }}')"
                                        action="{{ route(config('rbac.route_name') . 'role.destroy', $role) }}"
                                        method="post"
                                        class="d-inline-block"
                                    >
                                        @csrf
                                        @method('DELETE')
                                        <button
                                            class="btn btn-sm btn-outline-danger"
                                            type="submit"
                                        >
                                            <i class="fa-regular fa-trash-can"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
</div>
@endsection
