@extends(config('rbac.layout', 'rbac::layouts.app'))

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-auto">
                <div class="card">
                    <div class="card-header">{{ __('Rbac Actions') }}</div>

                    <div class="card-body">
                        <a href="{{ route(config('rbac.route_name') . 'action.create') }}" class="btn btn-primary">
                            {{ __('Create') }}
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="mt-5 row">
            <div class="col-md-8 mx-auto">
                @if(!count($newRouteActions))
                    @include('rbac::action.particles.actions-table')
                @else
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button
                            id="tabActions"
                            class="nav-link active"
                            data-bs-toggle="tab"
                            data-bs-target="#listActions"
                            type="button"
                            role="tab"
                            aria-controls="listActions"
                            aria-selected="true"
                        >
                            {{ __('Actions') }}
                        </button>
                    </li>

                    <li class="nav-item" role="presentation">
                        <button
                            id="tabNewActions"
                            class="nav-link"
                            data-bs-toggle="tab"
                            data-bs-target="#newActions"
                            type="button"
                            role="tab"
                            aria-controls="newActions"
                            aria-selected="false"
                        >
                            {{ __('New actions') }}
                            <i
                                class="fa-solid fa-circle-exclamation fa-beat ms-2 small text-warning"
                                style="--fa-animation-duration: 1.5s;"
                            ></i>
                        </button>
                    </li>
                </ul>

                <div class="mt-3 tab-content">
                    <div
                        class="tab-pane fade show active"
                        id="listActions"
                        role="tabpanel"
                        tabindex="0"
                    >
                        @if($actions->count())
                            @include('rbac::action.particles.actions-table')
                        @endif
                    </div>

                    <div
                        class="tab-pane fade"
                        id="newActions"
                        role="tabpanel"
                        tabindex="0"
                    >
                        @if(count($newRouteActions))
                            @include('rbac::action.particles.new-actions-table')
                        @endif
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection
