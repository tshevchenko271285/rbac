@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif

@if($successMessage = session('success'))
    <div class="alert alert-success">{{ $successMessage }}</div>
@endif

<div class="form-group">
    <label for="">{{ __('Action name') }} *</label>

    <input
        type="text"
        class="form-control"
        name="name"
        placeholder="{{ __('Action name') }}"
        value="{{ old('name', !empty($action) ? $action->name : '') }}"
    />
</div>
