<table class="table table-hover">
    <thead>
    <tr>
        <th>{{ __('Action name') }}</th>
        <th width="100"></th>
    </tr>
    </thead>

    <tbody>
    @foreach($actions as $action)
        <tr>
            <td class="{{ $action->trashed() ? 'text-secondary ' : '' }}">
                @if($action->trashed())
                    {{ $action->name }}
                @else
                    <a href="{{ route(config('rbac.route_name') . 'action.edit', $action) }}">
                        {{ $action->name }}
                    </a>
                @endif
            </td>

            <td>
                @if($action->trashed())
                    <form
                        onsubmit="return confirm('{{ __('Restore?') }}')"
                        action="{{ route(config('rbac.route_name') . 'action.restore', $action) }}"
                        method="post"
                        class="d-inline-block"
                    >
                        @csrf
                        @method('PATCH')
                        <button
                            class="btn btn-sm btn-outline-warning"
                            type="submit"
                        >
                            <i class="fa-solid fa-rotate-left"></i>
                        </button>
                    </form>
                @else
                    <a
                        href="{{ route(config('rbac.route_name') . 'action.edit', $action) }}"
                        class="btn btn-sm btn-outline-primary"
                    >
                        <i class="fa-solid fa-pencil"></i>
                    </a>
                @endif

                <form
                    onsubmit="return confirm('{{ __('Delete?') }}')"
                    action="{{ route(config('rbac.route_name') . 'action.destroy', $action) }}"
                    method="post"
                    class="d-inline-block"
                >
                    @csrf
                    @method('DELETE')

                    @if($action->trashed())
                        <button
                            class="btn btn-sm btn-danger "
                            type="submit"
                            value="{{ \Tshevchenko\Rbac\Http\Controllers\RbacActionController::FORCE_REMOVE_KEY }}"
                            name="{{ \Tshevchenko\Rbac\Http\Controllers\RbacActionController::FORCE_REMOVE_KEY }}"
                        >
                            <i class="fa-solid fa-xmark"></i>
                        </button>
                    @else
                        <button
                            class="btn btn-sm btn-outline-danger "
                            type="submit"
                        >
                            <i class="fa-regular fa-trash-can"></i>
                        </button>
                    @endif
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
