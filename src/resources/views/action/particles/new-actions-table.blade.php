<table class="table table-hover">
    <thead>
    <tr>
        <th>{{ __('#') }}</th>
        <th>{{ __('Action name') }}</th>
        <th width="100"></th>
    </tr>
    </thead>

    <tbody>
    @foreach($newRouteActions as $key => $newAction)
        <tr>
            <td>
                {{ $key + 1 }}
            </td>
            <td class="text-primary">
                {{ $newAction }}
            </td>

            <td class="action">
                <form
                    onsubmit="return confirm('{{ __('Are you sure?') }}')"
                    action="{{ route(config('rbac.route_name') . 'action.store') }}"
                    method="post"
                    class="d-inline-block"
                >
                    @csrf

                    <input type="hidden" value="{{ $newAction }}" name="name">

                    <button
                        class="btn btn-sm btn-outline-primary"
                        type="submit"
                    >
                        <i class="fa-solid fa-floppy-disk"></i>
                    </button>

                    <button
                        class="btn btn-sm btn-outline-danger"
                        type="submit"
                        value="{{ \Tshevchenko\Rbac\Http\Controllers\RbacActionController::REMOVE_KEY }}"
                        name="{{ \Tshevchenko\Rbac\Http\Controllers\RbacActionController::REMOVE_KEY }}"
                    >
                        <i class="fa-regular fa-trash-can"></i>
                    </button>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
