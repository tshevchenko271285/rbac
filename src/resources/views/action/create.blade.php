@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-auto mb-3">
                @include('rbac::action.particles.form-header', [
                    'title' => __('Create action'),
                ])
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 mx-auto">
                <form
                    class="form-horizontal"
                    action="{{ route(config('rbac.route_name') . 'action.store') }}"
                    method="post"
                >
                    {{ csrf_field() }}

                    @include('rbac::action.particles.form')

                    <hr/>

                    <a
                        href="{{ route(config('rbac.route_name') . 'action.index') }}"
                        class="btn btn-default"
                    >
                        {{ __('Cancel') }}
                    </a>

                    <input type="submit" class="btn btn-primary" value="{{ __('Save') }}">
                </form>
            </div>
        </div>
    </div>
@endsection
