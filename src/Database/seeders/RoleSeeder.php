<?php

namespace Tshevchenko\Rbac\Database\Seeders;

use Tshevchenko\Rbac\Models\RbacRole;
use Tshevchenko\Rbac\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * List of roles for seeding
     *
     * @var array
     */
    protected array $roles = [
        ['id'=> 1, 'title' => 'Super Admin'],
        ['id'=> 2, 'title' => 'Admin'],
        ['id'=> 3, 'title' => 'Manager'],
        ['id'=> 4, 'title' => 'User'],
    ];

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $permissions = config('rbac.permissions', []);

        foreach ($this->roles as $role) {
            $role = RbacRole::updateOrCreate($role, []);

            $rolePermissions = array_filter($permissions, fn($k, $p) => $k === $role->id, ARRAY_FILTER_USE_BOTH);
            $actionsID = [];
            foreach ($rolePermissions as $permission) {
                $action = $role->actions()->updateOrCreate(['name' => $permission['action']], []);
                $actionsID[] = $action->id;
            }

            $role->actions()->sync($actionsID);
        }

        $userModel = config('rbac.user_model');
        $users = $userModel::all();

        foreach ($users as $user) {
            if (!empty($user->email) && $user->email == config('rbac.super_admin_email')) {
                $role = RbacRole::findOrFail(1);
            } else {
                $role = RbacRole::findOrFail(4);
            }

            $role->users()->detach($user->id);
            $role->users()->attach($user->id);
        }
    }
}
